.. webchat documentation master file, created by
   sphinx-quickstart on Sun May 30 22:19:50 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Sohbet - Chat Sohbet Odaları Mobil Sohbet Sitesi!
==================================
https://sohbet.page Sohbet odaları irc alt yapısı ile oluşturulmuş metin tabanlı sohbet ve mirc chat seçeneği teknolojik gelişmeler sayesinde mobil sohbet siteleri entegrasyonu ile internet üzerinden hiç tanımadığınız kişiler ile kimliğiniz ve kişisel verileriniz gizlenmiş şekilde bir araya gelmenizi chat yaparak tanışmanızı, sohbet ederek hoş ve eğlenceli zaman geçirmenizi sağlamak amacı ile kurulmuştur. Farklı ülkelerden ve şehirlerden farklı kültürdeki kişilerin bir araya gelerek oluşturduğu sohbet odaları platformu içerisinde kendinize uygun şehirlerden yaş kategorinize göre kişilerle de karşılaşmanız mümkün. Bu büyük sosyal topluluk içerisinde genel ve özel sohbet seçeneklerini kullanırken site yönetimi tarafından belirlenen kurallara uyulması zorunludur.

.. toctree::
   :maxdepth: 2
   :caption: Contents:


Sponsor Bağlantılar
==================

* https://www.sohbet.ltd
* https://irc.ircask.com
* http://www.sohbet.network


